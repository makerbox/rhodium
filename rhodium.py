#!/usr/bin/env python
# based on http://mitchtech.net/raspberry-pi-pwm-rgb-led-strip/
# and http://elfboimakingstuff.tumblr.com/post/132956410578/raspberry-pi-pwm-rgb-led-strip
###
# Depends:
# [Pi-Blaster](https://github.com/sarfata/pi-blaster)
# [evdev](https://pypi.python.org/pypi/evdev)

from evdev import InputDevice, categorize, ecodes
from select import select
from time import sleep
import os
import sys
import string

#pins in use
rpin = 23
gpin = 24
bpin = 25
#values for LED colour
gamma = 2.2
rval = 255
gval = 255
bval = 255

def pwm(pin, angle):
    '''
    Send values to pi-blaster dev
    '''
    print("def pwm triggered")#DEBUG
    cmd = "echo " + str(pin) + "=" + str(angle) + " > /dev/pi-blaster"
    os.system(cmd)

def setledcolour(red, green, blue):
    '''
    Send ON signals thru pin to LED strip
    '''
    print("def setledcolour triggered")#DEBUG
    rfl = (float(red) / 255)**gamma
    gfl = (float(green) / 255)**gamma
    bfl = (float(blue) / 255)**gamma
    pwm(rpin,rfl)
    pwm(gpin,gfl)
    pwm(bpin,bfl)

if os.path.exists('/dev/pi-blaster'):
    print('found /dev/pi-blaster')
    setledcolour(0,0,0)
else:
    print('no pi-blaster dev found')

while True:
    try:
        print('here')
        if os.path.exists('/dev/input/event0'):
            dev = InputDevice('/dev/input/event0')
            #print("debug",dev,str(time()).split('.')[0])
            main = True
            while main == True:
                print('top ------------------------ ') #DEBUG

                r,w,x = select([dev], [], [])

                for event in dev.read():
                    print('for loop')
                    if event.type == ecodes.EV_KEY:
                        APRESS = categorize(event)
                        ADICT = str(APRESS).split(" ")
                        setledcolour(rval,gval,bval)
                        print(ADICT)[-1]
                        if ADICT[-1] == 'up' or not ADICT[-1]:
                            print("break")
                            setledcolour(0,0,0)
                            break


    except:
        # gives time to disconnect
        # keyboard and plug in makey
        print('null')
        sleep(9)
