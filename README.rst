Rhodium
===========

Rhodium is internal code for an art installation. Triggered by a key press or makey-makey, it illuminates LED strips.

This is largely a combination of the script found at `elfboimakingstuff <http://elfboimakingstuff.tumblr.com/post/132956410578/raspberry-pi-pwm-rgb-led-strip>`_ (itself based on the schematic at `mitchtech <http://mitchtech.net/raspberry-pi-pwm-rgb-led-strip/>`_) and `makerbox/regolith <https://gitlab.com/makerbox/regolith>`_.

This code is probably not all that useful to you if you are not working on this art project.


Dependencies
----------------

* `evdev <https://pypi.python.org/pypi/evdev>`_
* `Pi-Blaster <https://github.com/sarfata/pi-blaster>`_
