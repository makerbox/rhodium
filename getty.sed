# This is specifically for Raspian circa Debian 8.
# I'm pretty sure this is totally NOT sytemd standard,
# but the "Debian way". It's pretty annoying.
#
# Don't try to run this as a script. It's really just
# documentation. Do as it says, not as it does.

sed -i '/ExecStart=/s/^/#/' /lib/systemd/system/getty\@.service

sed -i 's_\[Service\]_\[Service\]\nExecStart=-/sbin/agetty --autologin root --noclear %I $TERM_' /lib/systemd/system/getty\@.service